//login
export const LOG_IN = 'LOG_IN';
export const LOG_IN_PENDING = 'LOG_IN_PENDING';
export const LOG_IN_SUCCESS = 'LOG_IN_SUCCESS';
export const LOG_IN_FAILURE = 'LOG_IN_FAILURE';
export const LOG_OUT = 'LOG_OUT';
export const LOG_OUT_SUCCESS = 'LOG_OUT_SUCCESS';

//user
export const CREATE_USER = 'CREATE_USER';
export const CREATE_USER_PENDING = 'CREATE_USER_PENDING';
export const CREATE_USER_SUCCESS = 'CREATE_USER_SUCCESS';
export const CREATE_USER_FAILURE = 'CREATE_USER_FAILURE';

export const UPDATE_USER = 'UPDATE_USER';
export const UPDATE_USER_SUCCESS = 'UPDATE_USER_SUCCESS';
export const UPDATE_USER_FAILURE = 'UPDATE_USER_FAILURE';

export const ACTIVE_USER = 'ACTIVE_USER';
export const ACTIVE_USER_SUCCESS = 'ACTIVE_USER_SUCCESS';
export const ACTIVE_USER_FAILURE = 'ACTIVE_USER_FAILURE';
//loading
export const LOADING = 'LOADING';

//courses
export const ADD_COURSE = 'ADD_COURSE';
export const ADD_COURSE_SUCCESS = 'ADD_COURSE_SUCCESS';
export const ADD_COURSE_FAILURE = 'ADD_COURSE_FAILURE';

export const GET_LIST_COURSE = 'GET_LIST_COURSE';
export const GET_LIST_COURSE_SUCCESS = 'GET_LIST_COURSE_SUCCESS';
export const GET_LIST_COURSE_FAILURE = 'GET_lIST_COURSE_FAILURE';

export const UPDATE_COURSE = 'UPDATE_COURSE';
export const UPDATE_COURSE_SUCCESS = 'UPDATE_COURSE_SUCCESS';
export const UPDATE_COURSE_FAILURE = 'UPDATE_COURSE_FAILURE';

export const DELETE_COURSE = 'DELETE_COURSE';
export const DELETE_COURSE_SUCCESS = 'DELETE_COURSE_SUCCESS';
export const DELETE_COURSE_FAILURE = 'DELETE_COURSE_FAILURE';
 
//category
export const GET_CATEGORIES = 'GET_CATEGORIES';
export const GET_CATEGORIES_SUCCESS = 'GET_CATEGORIES_SUCCESS';
export const GET_CATEGORIES_FAILURE = 'GET_CATEGORIES_FAILURE';

