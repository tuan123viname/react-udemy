import { takeLatest , put, call } from 'redux-saga/effects';

import { LOG_OUT } from '../../constant/ActionTypes';
import Axios from 'axios';
import { rootURL } from '../../constant/Api';
import { logoutSuccess } from '../../actions';

async function logoutAsync(){
    const res = await Axios.get( rootURL + '/logout');
    console.log(res.data.status);
    console.log(res.status);
    if( res.status == 200 )
    {
        
        return await res.data.status;
    }
    throw new Error(res.statusText);
}

function* logout( action ){
    try{
       const data = yield call(logoutAsync);
       yield put( logoutSuccess(data));
    }
    catch(err){
       yield alert('dang xuat that bai');
    }
}

export default function* watchLogout(){
    yield takeLatest(LOG_OUT, logout);
}
