import axios from 'axios';
import { put, takeLatest, call } from 'redux-saga/effects';
import { rootURL } from './../../constant/Api';
import { LoginSuccess, loginFailure, isLoading } from '../../actions';
import { LOG_IN } from '../../constant/ActionTypes';
async function loginAsync( data ) {
    const res = await axios.post ( rootURL+'/login',data);
    if(res.status == 200)
    {
        const token = res.headers['auth-token'];
        const data = { ...res.data, token };
        return data;
    }
    throw new Error(res.statusText);
    
}

function* login( action ){
    yield put( isLoading(true) );
    try{
        const data = yield call( loginAsync, action.data );
        yield put( isLoading(false) );
        yield put ( LoginSuccess(data) );
        
    }
    catch (err){
        yield put( isLoading(false) );
        yield put ( loginFailure('Email hoặc mật khẩu không đúng') );
    }
}

export default function* watchLogin(){
    yield takeLatest( LOG_IN, login ); 
}