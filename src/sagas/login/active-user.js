import axios from 'axios';
import { call , put , takeLatest } from 'redux-saga/effects';
import * as types from '../../constant/ActionTypes';
import { activeUserSuccess, activeUserFailure } from '../../actions';
import { rootURL } from '../../constant/Api';

async function activeAccountAsync( data ){
   
    const res = await axios.post(rootURL + '/active-account', data );
    if ( res.status == 200 )
    {
        
        return  res.data;
    }
    
}

function* activeAccount( action )
{
    try{
        const data = yield call( activeAccountAsync , action.data );
        yield put( activeUserSuccess( data ));
    }
    catch (err)
    {
        console.log( err.response.data.message );
        yield put( activeUserFailure(err.response.data.message) );
    }
}
export default function* watchActiveAccount(){
    yield takeLatest( types.ACTIVE_USER , activeAccount );
}
