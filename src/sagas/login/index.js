import { all } from 'redux-saga/effects';
import watchLogin from './login';
import watchActiveAccount from './active-user';
import watchLogout from './logout';
export default function* rootLogin(){
    yield all ( [ watchLogin(), watchActiveAccount() ,watchLogout() ]);
}