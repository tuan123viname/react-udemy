import axios from 'axios';

import { createUserPending , createUserSuccess , createUserFailure, isLoading}  from './../../actions';
import { rootURL } from '../../constant/Api';

import { put , takeLatest , call} from 'redux-saga/effects';

async function createUserAsync( user ){
    const res = await axios.post(`${ rootURL }/register`, user );
    console.log(res.status);
    if(res.status == 200)
    {
        const message = 'Tạo tài khoản thành công'
        return message;
    }
    throw new Error (res.statusText);
}

function* create( action ){
    yield put( createUserPending());
    yield put (isLoading(true));
    try {
        const data = yield call ( createUserAsync , action.user );
        yield put(isLoading(false));
        yield put(createUserSuccess( data ));
    }
    catch (e){
        yield put(isLoading(false));
        yield put (createUserFailure('Tạo tài khoản thất bại'));
    }
}

export default function* watchCreate() {
    yield takeLatest('CREATE_USER', create);
}