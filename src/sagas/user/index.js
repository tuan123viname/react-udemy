import { all } from 'redux-saga/effects';
import watchCreate from './create';
import watchUpdate from './update';

export default function*rootUser(){
    yield all( [ watchCreate(),  watchUpdate() ]);
}