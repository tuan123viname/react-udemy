import { put , call ,takeLatest, delay } from 'redux-saga/effects';
import axios from 'axios';
import * as types from '../../constant/ActionTypes';
import { updateUserSuccess, updateUserFailure, isLoading } from '../../actions';
import { rootURL } from '../../constant/Api';

async function updateUserAsync( userData  ){
    console.log(userData)
    const config = {
        headers : {
            'auth-token' : userData.token
        }
    }
   const res = await axios.put( rootURL + '/change-profile ', userData,config);
   let user ;
   if( res.status == 200)
    {
         user = { ... res.data.user , token: userData.token };
      
    }
    if( userData.image )
    {
        const formData = new FormData();
        formData.append ('image', userData.image );
        const resUpdateImage = await axios.put( rootURL + '/change-avatar', formData, config );
       resUpdateImage.status == 200 ? user = { ...user , image : resUpdateImage.data.user.image } : null; 
    }
    if( res.status == 200 )
    {
        return user;
    }
    
    throw new Error(res.data.message)
}

function* update( action ){
    yield put ( isLoading(true) );
    try {
        const data = yield call( updateUserAsync, action.data );
        yield put ( updateUserSuccess(data) );
        yield put ( isLoading(false) );
    }
    catch (err){
        console.log('error'+ JSON.stringify(err.response.data));
        yield put ( isLoading(false) );
        yield put ( updateUserFailure(err) );
    }
}

export default function* watchUpdate(){
  
  yield takeLatest ( types.UPDATE_USER , update);
}

