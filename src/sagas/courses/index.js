import { all } from 'redux-saga/effects';
import watchGetListCourse from './get';
export default function* rootCourses(){
    yield all([watchGetListCourse()]);
}