import Axios from 'axios';
import { put, call, takeLatest } from 'redux-saga/effects';
import { rootURL } from '../../constant/Api';
import { isLoading, getListCourseSuccess, getListCourseFailure } from '../../actions';
import { GET_LIST_COURSE } from '../../constant/ActionTypes';

async function getListCourseAsync( idUser){
    const res = await Axios.get( rootURL + '/course/getby-iduser/' + idUser );
    console.log(res);
    if( res.status == 200 )
    {
        return await res.data;
    }
    throw new Error( await res );
}

function* getListCourse( action ){
     yield put (isLoading( true ));
    try{
      const listCourse = yield call( getListCourseAsync,action.idUser );
        yield put(getListCourseSuccess( listCourse ));
        yield put (isLoading(false));
    }
    catch(err){
        yield put(isLoading(false));
        yield put(getListCourseFailure(err));
        console.log(err);
    }
}

export default function* watchGetListCourse(){
    yield takeLatest( GET_LIST_COURSE , getListCourse);
}