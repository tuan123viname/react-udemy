import Axios from "axios";
import { put, call, takeLatest } from 'redux-saga/effects';
import { rootURL } from "../../constant/Api";
import { getCategoriesSuccess, getCategoriesFailure } from "../../actions";
import { GET_CATEGORIES } from "../../constant/ActionTypes";

async function getCategoriesAsync()
{
   const res = await Axios.get( rootURL + '/category/get-all-category');
   if(res.status == 200)
   {
       return res.data;
    }
    throw new Error(res);
}
function* getCategories( action )
{
    try{
        const categories = yield call(getCategoriesAsync);
        yield put(getCategoriesSuccess(categories));
    }
    catch(err)
    {
        console.log(err);
        yield put(getCategoriesFailure(err.response.data))
    }
  
}

export default function* watchGetCategories(){
    yield takeLatest( GET_CATEGORIES , getCategories);
}