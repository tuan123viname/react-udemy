import { all } from 'redux-saga/effects';
import watchGetCategories from './get';
export default function* rootCategory(){
    yield all( [ watchGetCategories() ]);
}