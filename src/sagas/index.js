import { all } from 'redux-saga/effects';

import userSaga from './user';
import loginSaga from './login';
import courseSaga from './courses';
import categorySaga from './category';
export default function* rootSaga(){
    yield all ( [ userSaga(), loginSaga(), courseSaga(), categorySaga() ] );
}