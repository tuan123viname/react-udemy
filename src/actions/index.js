import * as types from '../constant/ActionTypes';

//user
export const createUserPending = () => { return { type : types.CREATE_USER_PENDING } };
export const createUserSuccess = ( data ) =>{ return { type : types.CREATE_USER_SUCCESS , data } };
export const createUserFailure = ( error ) =>{ return  {type : types.CREATE_USER_FAILURE , error } };

export const updateUserSuccess = (data) => { return { type : types.UPDATE_USER_SUCCESS , data }};
export const updateUserFailure = (error) => { return { type : types.UPDATE_USER_FAILURE, error }};

//login
export const loginPending = () => { return { type : types.LOG_IN_PENDING }};
export const LoginSuccess = ( data ) => {return { type : types.LOG_IN_SUCCESS,  data }};
export const loginFailure = ( error ) => { return { type : types.LOG_IN_FAILURE, error }};

export const activeUserSuccess = ( data ) => { return { type : types.ACTIVE_USER_SUCCESS , data } };
export const activeUserFailure = ( error ) => { return { type : types.ACTIVE_USER_FAILURE , error }};
//logout
export const logoutSuccess = () => { return { type : types.LOG_OUT_SUCCESS }};
//loading
export const isLoading = ( boolean  ) => { return { type: types.LOADING , data : boolean}};

//course
export const getListCourseSuccess = ( data ) => { return { type : types.GET_LIST_COURSE_SUCCESS , data }};
export const getListCourseFailure = ( error ) => { return { type : types.GET_LIST_COURSE_FAILURE , error }};

//category
export const getCategoriesSuccess = ( data ) => { return { type: types.GET_CATEGORIES_SUCCESS , data }};
export const getCategoriesFailure = ( error ) => { return { type : types.GET_CATEGORIES_FAILURE , error }};
