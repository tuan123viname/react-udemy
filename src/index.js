import 'regenerator-runtime/runtime';
import React from 'react';
import ReactDOM from 'react-dom';

import { createStore , applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';
import { Provider } from 'react-redux';

import rootSaga from './sagas';
import rootReducer from './reducers';

import Routing from './App';

const sagaMiddleware = createSagaMiddleware();
const store = createStore( rootReducer, applyMiddleware(sagaMiddleware) );
sagaMiddleware.run(rootSaga);


function App(props) {
    return (
        <Provider store = {store}>
            <Routing />
       </Provider>
    )
}

ReactDOM.render( <App />, document.getElementById('root'));