import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faThumbsUp, faThumbsDown } from '@fortawesome/free-solid-svg-icons';
import { Avatar, TextField } from '@material-ui/core';

import './style.css';

export default function CourseDiscuss(){
    return (
        <div className = 'course-discuss-container'>
            <div className = 'comment-box' >
                <div className = 'avt-box-comment'>
                    <Avatar src = 'https://material-ui.com/static/images/avatar/2.jpg' />
                </div>
                <div className = 'comment-content'>
                    <TextField fullWidth = { true } label = ' write something...' />
                    <div className = 'btn-comment'><button className = 'btn btn-success'>Bình luận</button></div>
                </div>
            </div>
            <ul className = 'course-discuss-list'>
                <li className = 'course-discuss-item'>
                    <div className = 'course-discuss-left'><Avatar src = 'https://material-ui.com/static/images/avatar/2.jpg' /></div>
                    <div className ='course-discuss-right' >
                        <div >
                            <span className = 'username-discuss'>vu ngoc tuan</span>
                            <span className = 'discuss-info'>2 thang truoc</span>
                        </div>
                        <div className = 'content-discuss'>
                        The input label "shrink" state isn't always correct. The input label is supposed to shrink as soon as the input is displaying something. In some circumstances, we can't determine the "shrink" state (number input, datetime input, Stripe input). You might notice an overlap.
                        </div>
                        <div className = 'discuss-reaction'>
                            <span><FontAwesomeIcon icon = { faThumbsUp } /> </span>
                            <span><FontAwesomeIcon icon = { faThumbsDown } /></span>
                            <span>Phản hồi</span>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    )
}