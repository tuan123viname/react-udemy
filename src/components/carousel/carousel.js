import React, { useState , useLayoutEffect, useRef, useEffect} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { faAngleLeft , faAngleRight} from '@fortawesome/free-solid-svg-icons';
import CourseCard from '../course-card/CourseCard';
import Skeleton from '@material-ui/lab/Skeleton';
import './style.css';

export default function Carousel(props){

    const targetRef = useRef();
    const [ dimensions, setDimensions ] = useState({ width:0, height: 0 });
    const [ marginLeft, setMarginLeft ] = useState(0);
    const [ carouselWidth,setCarouselWidth ] = useState(230*props.listCourse.length);
    const [ displayArrowLeft , setDisplayArrowLeft ] = useState('block');
    const [ displayArrowright , setDisplayArrowRight ] = useState('block');

    useEffect( () => {
        setCarouselWidth( 230*props.listCourse.length );
            if (targetRef.current) {
                setDimensions({
                    width: targetRef.current.offsetWidth,
                    height: targetRef.current.offsetHeight
                });
            }
    }, [ props.listCourse ]);

    const handleSlide = (width) => {
        if( marginLeft + width > 0 )
        {
            console.log(width);
            setMarginLeft(0);
            setDisplayArrowLeft('none');
            setDisplayArrowRight('block');
            
        }
        else if(width+marginLeft -dimensions.width < -carouselWidth)
        {
            setMarginLeft(-carouselWidth+dimensions.width);
            setDisplayArrowRight('none');
            setDisplayArrowLeft('block');
        }
        else
        {
            dimensions.width >500 ? setMarginLeft(width + marginLeft) : setMarginLeft(width + marginLeft + 90);
            
            setDisplayArrowLeft('block');
            setDisplayArrowRight('block');
        }
        
        
    }

    return (
        <div className="carousel-container">
            <span style = { {display : displayArrowLeft} } onClick = {() => handleSlide(dimensions.width+20)} className = "arrow-control arrow-control-left"> <FontAwesomeIcon  icon ={faAngleLeft} /></span>
            <span style = { {display : displayArrowright} } onClick = {()=> handleSlide(-dimensions.width-20)} className = "arrow-control arrow-control-right"><FontAwesomeIcon  icon ={faAngleRight} /></span>
            <div ref={targetRef} className="carousel-wrapper">
                <div style = {{marginLeft : marginLeft+'px',width : carouselWidth}} className="carousel-course" >
                    { props.listCourse.length === 0 ?[1,2,3,4,5].map(item =><Skeleton key = { item } variant = 'rect' width = { 210 } height = { 250 } /> )   : null }
                   { props.listCourse.map((item,index) => <CourseCard key={index} courseInfo = {item} /> ) }
        
                </div>
            </div>
        </div>
    )
}