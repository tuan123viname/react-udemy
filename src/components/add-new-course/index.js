import React, { useState, useRef, useEffect } from 'react';

import './style.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { connect } from 'react-redux';
import Axios from 'axios';
import { rootURL } from '../../constant/Api';
import { ADD_COURSE } from '../../constant/ActionTypes';

const AddNewCourse = ( props ) => {

    const [ course , setCourse ] = useState({
        name        : '',
        goal        : '',
        category    : '',
        price       : 0,
        discount    : 0,
        image       : null,
        description : ''
    });
    const imageCourse = useRef();

    function addCourse(){
        if ( !course.name || !course.goal  || !course.category || !course.description || !course.image )
        {
            alert (' Bạn phải nhập đầy đủ thông tin');
            return;
        }
        const formData = new FormData();
        for( let key in course)
        {
            formData.append( key, course[ key ]);
        }
        Axios.post( rootURL + '/course/create', formData, {
            headers : {
                'auth-token' : props.token,
                'Content-Type' : 'application/json'
            }
        })
        .then( res => {
            console.log('success'+ JSON.stringify(res.data));
            props.updateListCourse(res.data.newCourse );
            props.setOpenModal( false );
        })
        .catch( err => {
            console.log(err);
        })
    }

    return (
        <div className = 'new-course-container'>
            <div className = 'new-course-wraper' onClick = { () => props.setOpenModal(false) }>

            </div>
            <div className = 'new-course-form '>
                <div className = 'title'>
                    <span className='text-title'>Thêm một môn học mới</span>
                    <span onClick = { () => props.setOpenModal(false)} className = 'close-icon'><FontAwesomeIcon icon = { faTimes } /></span>
                </div>
               <div className = 'form-group'>
                    <input className = 'form-control' value = { course.name } onChange = { ( e ) => setCourse({ ...course, name : e.target.value })} placeholder = 'Tên môn học...' />
                    <select className = 'form-control'  onChange = { ( e ) => setCourse({ ...course, category : e.target.value })}>
                     { props.categories.map( (item) => <option key = { item._id }  value = { item._id }>{ item.name }</option>) }
                    </select>
                    <input className = 'form-control' placeholder = 'Mục tiêu...' onChange = { ( e ) => setCourse({ ...course, goal : e.target.value })} />
                    <textarea className = 'form-control ' placeholder = 'Mô tả...' onChange = { ( e ) => setCourse({ ...course, description : e.target.value })} />
                    <input className = 'form-control' type = 'number' min = '0' placeholder = 'Giá...' onChange = { ( e ) => setCourse({ ...course, price : parseInt(e.target.value) })} />
                    <input className = 'form-control' type = 'number' min= '0' max = '100' placeholder = 'Giảm giá(%)...' onChange = { ( e ) => setCourse({ ...course, discount : parseInt(e.target.value) })} />
                    <input ref = { imageCourse }  accept="image/*" onChange = { (e) => { setCourse( { ...course, image : imageCourse.current.files[0] })}} type = 'file' className = 'form-control-file'/>
                    <button onClick = { addCourse } className = 'btn btn-success'>Thêm khóa học</button>
               </div>
                
            </div>
        </div>
    )
}

const mapStateToProps = ( state ) => {
    return { 
        token : state.login.user.token
    }
  
}
const mapDispatchToProps = ( dispatch ) => {
    return {
        updateListCourse : ( course ) => dispatch( { type : ADD_COURSE , data : course })
    }
}

export default connect( mapStateToProps, mapDispatchToProps )( AddNewCourse );