import { makeStyles } from '@material-ui/core/styles';
 export const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
      },
      panelHead: {
        flexDirection : 'column',
      },
      heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '33.33%',
        flexShrink: 0,
      },
      secondaryHeading: {
        fontSize: 13,
        color: theme.palette.text.secondary,
      },
      listLecture : {
          listStyle : 'none',
          margin: 0,
          padding : 0,
          display : 'flex',
          flexDirection : 'column',
          width: '100%'
      }, 
      LectureItemInfo : {
        fontSize: 13,
        color: theme.palette.text.secondary,
        marginLeft: 50,
        marginBottom: 5
      },
      lectureCheckbox : {
          padding: '5px 10px 0'
      },
}));