import React  from 'react';
import { useStyles } from './style';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import PlayCircleFilledIcon from '@material-ui/icons/PlayCircleFilled';

import './style.css';

export default function ActionsInExpansionPanelSummary() {
    const classes = useStyles();
  

    return (
        <div className={classes.root}>
            
        <ExpansionPanel>
            <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-label="Expand"
            aria-controls="additional-actions1-content"
            id="additional-actions1-header"
            style = {{backgroundColor : '#f7f8fa'}}
            >
            <div className = { classes.panelHead }>
                <Typography  className = { classes.heading }>Section 1: introduction</Typography>
                <div className = { classes.secondaryHeading }> <span>1/2</span> | <span>2 minute</span></div>
            </div>
        
            </ExpansionPanelSummary>
            <ExpansionPanelDetails style = {{ padding: 0}}>
                <ul className = { classes.listLecture }>
                    <li className = 'list-lecture-item'>
                        <Checkbox className = { classes.lectureCheckbox} /> 1. introduction
                        <div className = { classes.LectureItemInfo }><PlayCircleFilledIcon style = {{ fontSize : 16 }} />1 minute</div>
                    </li>
                    <li className = 'list-lecture-item' >
                        <Checkbox className = { classes.lectureCheckbox} /> 2. install 
                        <div className = { classes.LectureItemInfo }><PlayCircleFilledIcon style = {{ fontSize : 16 }} />1 minute</div>
                    </li>
                </ul>
            </ExpansionPanelDetails>
        </ExpansionPanel>

        <ExpansionPanel>
            <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-label="Expand"
            aria-controls="additional-actions1-content"
            style = {{backgroundColor : '#f7f8fa'}}
            id="additional-actions1-header"
            >
            <div className = { classes.panelHead }>
                <Typography  className = { classes.heading }>Section 2: Started</Typography>
                <div className = { classes.secondaryHeading }> <span>1/2</span> | <span>2 minute</span></div>
            </div>
        
            </ExpansionPanelSummary>
            <ExpansionPanelDetails style = {{ padding: 0}}>
                <ul className = { classes.listLecture }>
                    <li className = 'list-lecture-item'>
                        <Checkbox className = { classes.lectureCheckbox} /> 1. introduction
                        <div className = { classes.LectureItemInfo }><PlayCircleFilledIcon style = {{ fontSize : 16 }} />1 minute</div>
                    </li>
                    <li className = 'list-lecture-item' >
                        <Checkbox className = { classes.lectureCheckbox} /> 2. install 
                        <div className = { classes.LectureItemInfo }><PlayCircleFilledIcon style = {{ fontSize : 16 }} />1 minute</div>
                    </li>
                </ul>
            </ExpansionPanelDetails>
        </ExpansionPanel>

        </div>
    );
    }