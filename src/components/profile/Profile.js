import React, { useState, Fragment , useRef, useEffect } from 'react';
import { Link , useParams } from 'react-router-dom';
import { rootURL } from '../../constant/Api';

import Cart from '../card/Card';
import './style.css';
import Axios from 'axios';

export default function Profile(props){
   
    const imageFile = useRef();
    const { name , phone, gender , description, address, id ,token } = props.userInfo;
    const [ userData , setUserData ] = useState ({
        id,
        name,
        phone,
        gender,
        description,
        address,
        image : '',
        token
    });
    const [ showAlert , setShowAlert ] = useState ( 'none' ); 
    const [ tokenActive , setTokenActive ] = useState('');
    const [ oldpassword , setOldpassword ] = useState('');
    const [ newpassword , setNewpassword ] = useState (' ');

    useEffect( () => {
        console.log(userData);
         handleShowAlert();
     }, [ props.message , props.error ]);
 

    function handleShowAlert (){
            setShowAlert('block');
            setTimeout( () => {
                setShowAlert('none');
            }, 3000 );
    }

    function handleUpdateUser(){
      if( !userData.name || !userData.phone || !userData.description || !userData.gender)
      {
          alert ('Bạn vui lòng nhập đủ thông tin');
          return;
      }
      
      props.updateUser( userData );


    }

    function changePassword(data){
        console.log( props.userInfo.token)
        Axios.put( rootURL + '/change-password',data ,{
            headers : {
                'auth-token': props.userInfo.token
            }
        })
        .then(res => {
            console.log(res);
            setNewpassword('');
            setOldpassword('');
            alert( 'Đổi mật khẩu thành công' );
        })
        .catch(err => {
            console.log(err);
            alert (err.response.data.status );
        })
    }

    const profileTab = <Fragment>
                <div className="profile-tab-content">
                    <h3 className="heading">Hồ sơ công khai</h3>
                    <div className = "alert alert-success " style = {{display : showAlert }} >{ props.message }</div>
                    <p>Thông tin cơ bản</p>
                    <label>Tên</label><input className="form-control" onChange = { (e) => setUserData({ ...userData , name : e.target.value})} defaultValue={props.userInfo.name} placeholder="Tên" />
                    <label>Email</label><input onChange = { (e) => setUserData({ ...userData , email : e.target.value})} className="form-control" defaultValue={props.userInfo.email} placeholder="Email" disabled />
                    <label>Số điện thoại</label><input onChange = { (e) => setUserData({ ...userData , phone : e.target.value})} className="form-control" defaultValue={props.userInfo.phone} placeholder="Số điện thoại" />
                    <label>Địa chỉ</label><input className="form-control" onChange = { (e) => setUserData({ ...userData , address : e.target.value})} defaultValue={props.userInfo.address} placeholder="Địa chỉ" />
                    <label>Về bạn</label><input onChange = { (e) => setUserData({ ...userData , description : e.target.value})} className="form-control" defaultValue={props.userInfo.description} placeholder="Mô tả..." />
                    <label>Giới tính</label><select onChange = { (e) => setUserData({ ...userData , gender : e.target.value})} defaultValue={ props.userInfo.gender } className="form-control">
                        <option value = "Nam">Nam</option>
                        <option value = "Nữ">Nữ</option>
                    </select>
                    <label>Ảnh đại diện</label><input accept=" image/* " type="file" onChange = { () => setUserData({ ...userData , image : imageFile.current.files[0] })} ref = { imageFile } className="form-control-file" />
                    <button className="btn btn-success mt-3" onClick = { handleUpdateUser } >Cập nhật</button>
                </div>
    </Fragment>

    const myCoursesTab = <Fragment>
            <div className="profile-tab-content" >
                <h3 className="heading">Khóa học của tôi</h3>
                <div className="my-courses">
                    { props.courses.map((item,index) => <Cart course = { item } key={ index } /> ) }
                   
                </div>
            </div>
    </Fragment>

    const activeAccountTab = <Fragment>
        <div className = 'profile-tab-content'>
            <h3 className = 'heading'>Xác thực tài khoản</h3>
            <div className = "alert alert-danger " style = {{display : showAlert }} >{ props.error }</div>
            { 
                props.userInfo.active == 1 ? 
                <p>Tài khoản của bạn đã được xác thực</p>:
                <div className = "form-group">
                    <p>Chúng tôi đã gửi mã token đến email của bạn, bạn vui lòng kiểm tra mail để lấy token.</p>
                    <input onChange = { (e) => setTokenActive( e.target.value )} type="text" className="form-control" placeholder='Token...' />
                    <button className = 'btn btn-success' onClick = { () => props.activeAccount({ email : props.userInfo.email, activeToken: tokenActive}  )}>Xác thực</button>
                </div>
            }
            
        </div>
    </Fragment>

    const changePasswordTab = <div className = 'profile-tab-content'>
    <h3 className = 'heading'>Đổi mật khẩu</h3>
    <div className = "form-group">
        <input onChange = { (e) => setOldpassword( e.target.value )} type="text" className="form-control" value = { oldpassword } placeholder='Mật khẩu cũ...' />
        <input onChange = { (e) => setNewpassword( e.target.value )} type="text" className="form-control" value = { newpassword } placeholder='Mật khẩu mới...' />
        <button className = 'btn btn-success' onClick = { () => changePassword({ oldpassword , newpassword }  )}>Xác nhận</button>
    </div>
</div>

    function handleSwitchTab(){
        switch ( props.activeTab )
        {
            case 'my-profile' :
                return profileTab;
            case 'my-course' : 
                return myCoursesTab;
            case 'active-account':
                return activeAccountTab;
            case 'change-password': 
                return changePasswordTab;
            default:
                return profileTab;
        }
    }

    return (
        <div className="profile-container">
            <ul className="profile-tab">
                <div className="profile-avt">
                    <img src = { `${ rootURL }/upload/user_image/${ props.userInfo.image }`} />
                    <p className="name">{props.userInfo.name}</p>
                </div>
                    <Link to = '/profile/my-profile'>
                    <li className={ props.activeTab == 'my-profile' ? 'tab-active' : null } >
                       
                            Hồ sơ cá nhân
                        
                    </li>
                    </Link>
                    <Link to = '/profile/my-course'>
                        <li className={ props.activeTab == 'my-course' ? 'tab-active' : null } >
                            Các khóa học bạn tham gia
                        </li>
                    </Link>
                    <Link to = '/profile/active-account'>
                        <li className={ props.activeTab == 'active-account' ? 'tab-active' : null } >
                            Xác thực tài khoản
                        </li>
                    </Link>
                    <Link to = '/profile/change-password'>
                        <li className={ props.activeTab == 'change-password ' ? 'tab-active' : null } >
                            Đổi mật khẩu
                        </li>
                    </Link>

                   
              
              
            </ul>
           {
               handleSwitchTab()
           }
            
        </div>
    )
}