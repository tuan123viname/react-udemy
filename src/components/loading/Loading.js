import React , { useState } from 'react';
import './style.css';
import loadingImg from './../../assets/image/loading.gif';

export default function Loading(props){
    return (
        <div className="loading-container" style = { props.loading ? {display: 'block'}: null }>
            <img src={loadingImg} />
        </div>
    )
}