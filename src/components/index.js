import Header from './header/Header';
import LoginForm from './login-form/LoginForm';
import SignupForm from './signup-form/SignupForm';
import Profile from './profile/Profile';
import Card from './card/Card';
import Footer from './footer/Footer';
import TopBar from './top-bar/TopBar';
import CourseCard  from './course-card/CourseCard';
import Carousel from './carousel/carousel';
import CollapseLesson from './collapse-lesson/CollapseLesson';
import Loading from './loading/Loading';
import Video from './video';
import TableContent from './table-content';
import TabPanel from './TabPanel';
import Exercise from './exercise';
import CourseDiscuss from './course-discuss';
import CourseCardTeaching from './course-card-teaching';
import AddNewCourse from './add-new-course';
import UpdateCourse from './update-course';
export{
    Header,
    LoginForm,
    SignupForm,
    Profile,
    Card,
    Footer,
    TopBar,
    CourseCard,
    Carousel,
    CollapseLesson,
    Loading,
    Video,
    TableContent,
    TabPanel,
    Exercise,
    CourseDiscuss,
    CourseCardTeaching,
    AddNewCourse,
    UpdateCourse
}