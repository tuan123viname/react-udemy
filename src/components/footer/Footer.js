import React from 'react';
import './style.css';

export default function Footer(props){
    return (<div className="footer">
        <div className="logo-footer">
            <img src = "https://www.udemy.com/staticx/udemy/images/v6/logo-coral.svg" />
        </div>
        <ul className="footer-description">
            <h5>Thông tin liên lạc</h5>
            <li>Địa chỉ : ktx khu A</li>
            <li>Số điện thoại: 0967537483</li>
            <li>Email : vungoctuan9a6dt@gmail.com</li>
        </ul>
        <ul className="footer-memer">
            <h5>Dream team</h5>
            <li>Vũ Ngọc tuấn</li>
            <li>Cao Hoàng Tú</li>
            <li>Phạm Lam Trường</li>
            <li>Quang Thiên</li>
        </ul>
    </div>)
}