import React, {useState , Fragment, useEffect} from 'react';
import { connect } from 'react-redux';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faEnvelope, faLock} from '@fortawesome/free-solid-svg-icons';

import './style.css';
import { ValidateEmail } from '../../helper';
import { LOG_IN } from '../../constant/ActionTypes';
import Loading from '../loading/Loading';

 function LoginForm(props){
    const [ email , setEmail ] = useState('');
    const [ password , setPassword ] = useState('');
    const [ displayError , setDisplayError ] = useState('none');
    const [ error , setError ] = useState( props.error || '');
    useEffect ( () => {
        setError( props.error );
        handleError();
    }, [ props.error  ])

    function handleKeyPress( e ){
        e.key === 'Enter' ? handleLogin() : null ;
    } 


    function handleLogin(){
       if ( !ValidateEmail(email) )
       {
         setError('Email không hợp lệ.');
         
         handleError();
           return;
       }
       if ( password.length < 6)
       {
           setError('Mật khẩu phải đủ 6 ký tự trở lên');
           handleError();
           return;
       }
       props.login( { email , password });
     
      
    }
    function handleError(){
        if( error )
        {
            setDisplayError( 'block' );
            setTimeout( () => setDisplayError('none') , 3000 ) ;
        }
     
    }
    return (
        <Fragment>
            
            <div className="login-box" style={{display : props.displayLogin ? 'block' : 'none'}}>
                <h3>Đăng nhập vào tài khoản Udemy của bạn</h3>
                <div className="alert alert-danger" style = {{ display : displayError }} >{ error }</div> 
                <div className="form-group form-login">
                    <div className="form-control"><FontAwesomeIcon color="#4CAF50" icon={faEnvelope} /><input type="text" onChange = { (e) => setEmail(e.target.value)} value = { email } placeholder="Email..." onKeyPress = { handleKeyPress }  /></div>
                    <div className="form-control"><FontAwesomeIcon color="#4CAF50" icon={faLock} /><input type="password" value = { password } onChange = { (e) => setPassword(e.target.value) } placeholder="Mật khẩu..." onKeyPress = { handleKeyPress } /></div>
                   <div className="form-control"><button onClick = { handleLogin }  className="btn btn-login">Đăng nhập</button></div> 
                </div>
            </div>
            <Loading loading = {props.loading} />
        </Fragment>
    )
}

const mapStateToProps = (state) => {
    const { loading } = state.loading;
    const user = state.login.user;
    const error = state.login.error;
    const isLogedIn = state.login.isLogedIn;
    return { loading , user, error ,isLogedIn };
}
const mapDispatchToProps = ( dispatch ) => {
    return { login : (data) => dispatch({ type : LOG_IN, data })}
}

export default connect( mapStateToProps, mapDispatchToProps)(LoginForm);