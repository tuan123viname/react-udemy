import React, { useState , Fragment, Suspense, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch ,faGripHorizontal, faHeart, faShoppingCart,faBell,faBars } from '@fortawesome/free-solid-svg-icons';
import { connect } from 'react-redux';

import './style.css';
import Drawer from '../drawer/Drawer';
import SignupForm from '../signup-form/SignupForm';
import { rootURL } from '../../constant/Api';
import { GET_CATEGORIES } from '../../constant/ActionTypes';
const LoginForm = React.lazy(()=> import('../login-form/LoginForm'))  ;
// import LoginForm from '../login-form/LoginForm';
function Header( props ){
    const [ displayDrawer, setDisplayDrawer ] = useState(false);
    const [ displayLogin , setDisplayLogin] = useState(false);
    const [ displaySignup, setDisplaySignup] = useState(false);
    const [ user , setUser ] = useState( props.user.user || {} );
    useEffect( () => {
       if ( props.user.isLogedIn && displayLogin === true)
       {
           setDisplayLogin(false);
       }
       setUser( props.user.user );
       console.log(props.user.user);
       if( props.categories.length == 0)
       {
            props.getCategories();
       }
     
    } , [ props.user.isLogedIn , props.user.user ]);

    const handleDrawer = () => {
        setDisplayDrawer(true);
    }

    const login_signup = <li className="btn-login-signup"><button onClick={()=>setDisplayLogin(true)} className="btn login-btn">Đăng nhập</button><button onClick={() => setDisplaySignup(true)} className="btn signup-btn">Đăng ký</button></li>;

    const userDropdown = <Fragment>
      
                        <li className="favorite"><FontAwesomeIcon icon={faHeart} color="red" className="icon-top-nav"/></li>
                        
                        <li className="notification"><FontAwesomeIcon className="icon-top-nav" icon = {faBell} /></li>
        <li className="user-dropdown">
           <Link to='/profile/my-profile'> <img className="user-avt-image" src = {`${ rootURL }/upload/user_image/${ user.image }`}/></Link>
            <ul className="list-item-user">
                <li>
                    Notification
                </li>
                <li>Message</li>
                <Link to = '/forgot-password'>
                    <li>
                        Quên mật khẩu
                    </li>
                </Link>
                <li onClick = { props.logout } >Đăng xuất</li>
            </ul>
        </li>
    </Fragment>
    return (
        <div>
            <div className="header">
                <ul className = "top-nav">
                    <li className = "icon-menu-mb" onClick={ () => handleDrawer() }><FontAwesomeIcon className="icon-top-nav" icon={faBars} /></li>
                    <li className="logo" ><Link to="/"><img className="img-logo " src = "https://www.udemy.com/staticx/udemy/images/v6/logo-coral.svg" /></Link></li>
                    <li className="category-top">
                        <FontAwesomeIcon className="icon-top-nav" icon={ faGripHorizontal } /> Danh mục
                        <ul className="list-category-menu">
                            { props.categories.map( item => <li key = { item._id }>{ item.name }</li>)}
                        </ul>
                     </li>
                    <li className="cart"><FontAwesomeIcon className="icon-top-nav" icon = { faShoppingCart } /></li>
                    <li><input className="search-top" type="text" /><button className="icon-search"><FontAwesomeIcon  icon={faSearch} /></button></li>
                    <li className="teaching">Dạy trên Udemy</li>
                    {
                        props.user.isLogedIn ?  <li className = "my-course"> <Link to = '/my-courses'>Khóa học của tôi<i className="fa fa-angle-down"></i></Link></li>  : null
                    }
                    
                   {
                       props.user.isLogedIn ? userDropdown : login_signup
                   }
                </ul>           
            </div>

            {/* drawer */}
            <Drawer userInfo = { user }  display = {displayDrawer} isLogin={ props.user.isLogedIn } displayLogin = { setDisplayLogin } displaySignup = { setDisplaySignup } />
            <div onClick={()=>setDisplayDrawer(false)} className="drawer-wraper" style={{display : displayDrawer ? 'block' : 'none'}}></div>

            
           {/* display login form*/  }
           {
            props.user.isLogedIn ? null :  
            <Suspense fallback="<div>loading...</div>">
                 <div onClick={ ()=> setDisplayLogin(false)} className="wraper-login" style={{display : displayLogin ? 'block' : 'none'}} ></div>
                <LoginForm displayLogin = { displayLogin } setDisplayLogin = { setDisplayLogin } />
            </Suspense>
            }
           
            {/* display signup form*/  }
           { props.user.isLogedIn ? null : 
           <Fragment>
               <div onClick={ ()=> setDisplaySignup(false)} className="wraper-login" style={{display : displaySignup ? 'block' : 'none'}} ></div>
                <SignupForm signup = { props.create } display={ displaySignup } />
            </Fragment>
            }
            
            
        </div>
    )
}

const mapStateToProps = ( state ) => {
    return {
        user : state.login,
        categories : state.category.categories
    }
}

const mapDispatchToProps = ( dispatch ) => {
    return {
        create : ( user ) => dispatch( {type : 'CREATE_USER', user }),
        logout : () => dispatch ({ type : 'LOG_OUT' } ),
        getCategories : () => dispatch({ type : GET_CATEGORIES })
    }
}

export default connect( mapStateToProps , mapDispatchToProps )( Header ) ;