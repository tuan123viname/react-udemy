import React ,{ useState } from 'react';
import { Player } from 'video-react';
import 'video-react/dist/video-react.css';

import './style.css';

function Video(props){
    return (
        <Player fluid = {false} width = '100%' height = { props.height }>
            <source src = "https://media.w3.org/2010/05/sintel/trailer_hd.mp4" />
        </Player>   
    )
}
export default Video;