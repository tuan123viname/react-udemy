import React from 'react';
import StarRatings from 'react-star-ratings';
import './style.css'
import { rootURL } from '../../constant/Api';

export default function CourseCard(props){
    const courseInfo = props.courseInfo;
    return (
        <div className="course-card">
            <div className="course-card-top">
               {/* {courseInfo.label ? <span className="badge badge-success label-course">{courseInfo.label}</span> : null } */}
                <img src={ `${ rootURL }/upload/image_course/${ courseInfo.image }` } onError={(e)=>{e.target.onerror = null; e.target.src="https://picsum.photos/200/300"}}  />
                <div className="thumbnail-wraper"></div>
            </div>
            <div className="course-card-bot">
                <h5>{ courseInfo.name }</h5>
                <p>{ courseInfo.idUser.name }</p>
    <div className="mb-2"><StarRatings starDimension="15px" starSpacing="2px" changeRating = {props.changeRating} starRatedColor="#f4c150"  rating = { courseInfo.vote.EVGVote } numberOfStars={5} /><strong> { courseInfo.vote.EVGVote }</strong><span style = {{ color: '#686f7a',fontSize: '14px'}}>( { courseInfo.vote.totalVote } )</span></div>
                <div style={{textAlign:'right'}}>
                    { courseInfo.discount !== 0 ? <label className="price-old">{ courseInfo.price } đ</label> : null}
                    {
                        courseInfo.price == 0 ? <label className="price-new">Free</label> : <label className="price-new">{ courseInfo.price * ( 100 - courseInfo.discount ) / 100 } đ</label>
                    } 
                </div>
            </div>
        </div>
    )
}