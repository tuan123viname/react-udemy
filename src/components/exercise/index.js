import React , { useState, useLayoutEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { Step, Stepper, StepLabel, StepContent } from '@material-ui/core';

import { useStyle } from './style';
import './style.css';

export default function Exercise(props) {
    const classes = useStyle();
    const { isOpen, setIsOpen } = props;
    const [ answers, setAnswers ] = useState( new Map() );
    const [ currentQuestion , setCurrentQuestion ] = useState(1);
    const [ activeQuestion , setActiveQuestion ] = useState (0);

    function handleStep( step ){
        step <= activeQuestion + 1 ? setCurrentQuestion( step ) : null;
    }
    function handleQuestion( nextQuestion, ans ){
        setAnswers( answers.set( `q${nextQuestion - 1}`, ans ) );
        console.log(answers)
        setCurrentQuestion(nextQuestion);
        activeQuestion < nextQuestion ? setActiveQuestion(nextQuestion-1) : null;
    }

    return (
        <div className = 'exercise-container' style = { isOpen ? { height : '100%' } : null }>
            <div className = 'exercise-head'>
                <div className = 'exercise-title'>
                    Bai tap trac nghiem chuong 1    
                </div>
                <div className = 'exercise-close'>
                    <span onClick = { () => setIsOpen( false )} ><FontAwesomeIcon icon = { faTimes } /> </span>
                </div>
            </div>
            <div className = 'exercise-body'>
                <div className = 'exercise-body-left'>
                    <Stepper orientation = 'vertical' activeStep = { activeQuestion }>
                        <Step  className = { classes.stepExercise }  onClick = { () => handleStep(1)}>
                            <StepLabel><span className = { currentQuestion == 1 ? 'active-step' : null }>cau 1</span></StepLabel>
                        </Step>
                        <Step className = { classes.stepExercise} onClick = { () => handleStep(2)}>
                            <StepLabel><span className = { currentQuestion == 2 ? 'active-step' : null }>cau 2</span></StepLabel>
                        </Step>
                        <Step className = { classes.stepExercise} onClick = { () => handleStep(3)}>
                            <StepLabel><span className = { currentQuestion == 3 ? 'active-step' : null }>cau 3</span></StepLabel>
                        </Step>
                        <Step className = { classes.stepExercise} onClick = { () => handleStep(4)}>
                            <StepLabel><span className = { currentQuestion == 4 ? 'active-step' : null }>cau 4</span></StepLabel>
                        </Step>
                    </Stepper>
                </div>
                <div className = 'exercise-body-right'>
                    <div style = { 1 == currentQuestion ? {display : 'block'}: null } className = 'exercise-answer'>
                        <h3 className = 'exercise-question'> who am i ? </h3>
                        <ul className = 'list-answer'>
                            <li className = { answers.get('q'+1) == 1 ? 'exercise-answer-active' : null } onClick = { () => handleQuestion (2, 1 ) }>Dap an A </li>
                            <li className = { answers.get('q'+1) == 2 ? 'exercise-answer-active' : null } onClick = { () => handleQuestion(2,2) }>Dap an B</li>
                            <li className = { answers.get('q'+1) == 3 ? 'exercise-answer-active' : null } onClick = { () => handleQuestion(2,3) }>Dap an C </li>
                            <li className = { answers.get('q'+1) == 4 ? 'exercise-answer-active' : null } onClick = { () => handleQuestion(2,4) }>Dap an D</li>
                        </ul>
                    </div>
                    <div style = { 2 == currentQuestion ? {display : 'block'}: null } className = 'exercise-answer'>
                        <h3 className = 'exercise-question'> where are you from ? </h3>
                        <ul className = 'list-answer'>
                            <li className = { answers.get('q'+2) == 1 ? 'exercise-answer-active' : null } onClick = { () => handleQuestion(3,1)}>Dap an A </li>
                            <li className = { answers.get('q'+2) == 2 ? 'exercise-answer-active' : null } onClick = { () => handleQuestion(3,2)}>Dap an B</li>
                            <li className = { answers.get('q'+2) == 3 ? 'exercise-answer-active' : null } onClick = { () => handleQuestion(3,3)}>Dap an C </li>
                            <li className = { answers.get('q'+2) == 4 ? 'exercise-answer-active' : null } onClick = { () => handleQuestion(3,4)}>Dap an D</li>
                        </ul>
                    </div>
                    <div style = { 3 == currentQuestion ? {display : 'block'}: null } className = 'exercise-answer'>
                        <h3 className = 'exercise-question'> how old are you ? </h3>
                        <ul className = 'list-answer'>
                            <li className = { answers.get('q'+3) == 1 ? 'exercise-answer-active' : null } onClick = { () => handleQuestion(4,1)}>Dap an A </li>
                            <li className = { answers.get('q'+3) == 2 ? 'exercise-answer-active' : null } onClick = { () => handleQuestion(4,2)}>Dap an B</li>
                            <li className = { answers.get('q'+3) == 3 ? 'exercise-answer-active' : null } onClick = { () => handleQuestion(4,3)}>Dap an C </li>
                            <li className = { answers.get('q'+3) == 4 ? 'exercise-answer-active' : null } onClick = { () => handleQuestion(4,4)}>Dap an D</li>
                        </ul>
                    </div>
                    <div style = { 4 == currentQuestion ? {display : 'block'}: null } className = 'exercise-answer'>
                        <h3 className = 'exercise-question'> how are you ? </h3>
                        <ul className = 'list-answer'>
                            <li className = { answers.get('q'+4) == 1 ? 'exercise-answer-active' : null } onClick = { () => handleQuestion(5,1)}>Dap an A </li>
                            <li className = { answers.get('q'+4) == 2 ? 'exercise-answer-active' : null } onClick = { () => handleQuestion(5,2)}>Dap an B</li>
                            <li className = { answers.get('q'+4) == 3 ? 'exercise-answer-active' : null } onClick = { () => handleQuestion(5,3)}>Dap an C </li>
                            <li className = { answers.get('q'+4) == 4 ? 'exercise-answer-active' : null } onClick = { () => handleQuestion(5,4)}>Dap an D</li>
                        </ul>
                    </div>
                    <button  className ='btn btn-success'>Nộp bài</button>
                </div>
            </div>
        </div>
     
    )
}