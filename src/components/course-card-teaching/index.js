import React, { useLayoutEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrashAlt } from '@fortawesome/free-solid-svg-icons';

import './style.css';
import { rootURL } from '../../constant/Api';
import Axios from 'axios';
import { UpdateCourse } from '..';

function CourseCardTeaching(props){
    const [ openModalUpdate , setOpenModalUpdate ] = useState( false );
    function handleDeleteCourse(){
      
        const confirm = window.confirm('Bạn có chắc chắn muốn xóa khóa học này không?');
        if( confirm )
        {
            Axios.delete( rootURL + '/course/delete/' + props.course._id , {
                headers : {
                    'auth-token' : props.token
                }
            })
            .then( res => {
                if( res.data.status == 'success')
                {
                    props.deleteCourse( props.course._id );
                    alert('Xóa khóa học thành công');
                }
                else
                {
                    alert('Xóa khóa học thất bại');
                }
            })
            .catch( err => {
                console.log( err );
            })
           
        }
        return;
    }

    return (
        <div className = 'course-card-teaching' >
            { openModalUpdate ? <UpdateCourse openModalUpdate = { setOpenModalUpdate } categories = { props.categories } course = { props.course } updateCourse = { props.updateCourse } /> : null }
            <div className = 'overlay'>
                <span onClick = { () =>  setOpenModalUpdate(true) }><FontAwesomeIcon icon = { faEdit } /> Update</span>
                <span onClick = { handleDeleteCourse }><FontAwesomeIcon icon = { faTrashAlt } /> Remove</span>
            </div>
            <div className='thumbnail-card' >
                <div className = 'thumbnail-overlay'>

                </div>
                <img src = { rootURL + '/upload/course_image/'+props.course.image } />
            </div>
            <div className = 'info'>
                <h5> { props.course.name }</h5>
            </div>
        </div>

    )
}

export default CourseCardTeaching;