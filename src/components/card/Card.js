import React , {useState} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faStar, faPlay} from '@fortawesome/free-solid-svg-icons';
import './style.css';

export default function Card(props){
    const course = props.course;

    return (
        <div className="card-course">
           <div className="card-image">
                <div className = "icon-play">
                 <span><FontAwesomeIcon icon={faPlay} size="2x" style={{color: 'white'}} /></span>
                </div>
                <img src ={course.thumbnail} />
           </div>
           <div className="card-info">
                <h4>{course.name}</h4>
                <p>{course.teacher}</p>
               
                <div className="progress">
                   <div className="progress-bar" role="progressbar" style={{width: `${course.progress}%`,height: "10px"}} aria-valuenow={course.progress} aria-valuemin="0" aria-valuemax="100">{course.progress}%</div>
                </div>
                <div className="star">
                    {
                        [1,2,3,4,5].map(item =><FontAwesomeIcon key={item} className={item <= course.vote ? 'star-active icon-star' : 'icon-star'}  icon={faStar} />)
                    }
                    
                </div>
            </div>
        </div>
        
    )
}