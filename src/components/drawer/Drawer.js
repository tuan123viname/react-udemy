import React , {useState, Fragment} from 'react';
import { Link } from 'react-router-dom';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faShoppingCart, faPray} from '@fortawesome/free-solid-svg-icons';

import { rootURL } from '../../constant/Api';
import './style.css';
export default function Drawer(props){

    const user = <div className="drawer-head drawer-section">
                    <div className="avt-thumbnail" > 
                    <Link to = '/profile/my-profile'>
                        <img src = { `${ rootURL }/upload/user_image/${ props.userInfo.image }` } />
                    </Link> 
                    </div>
                    <div className="username-info">
                    <Link to = '/profile/my-profile'> <h3>{ props.userInfo.name } </h3></Link>
                        <p >Trang thai</p>
                    </div>
                </div>;
        const loginSignup = <Fragment>
           <div> <button className="btn btn-success mr-2" onClick={ () => props.displayLogin(true) }>Đăng nhập</button>
            <button className="btn btn-danger" onClick={() => props.displaySignup(true) }>Đăng ký</button></div>
        </Fragment>

    return (
        <div>

            <div>
                <div className="drawer" style={{ marginLeft : props.display ? '0px' : '-300px'}}>
                    
                       {
                           props.isLogin ? user : loginSignup
                       }
                
                    <div>
                         <div className="cart-drawer"><FontAwesomeIcon className="icon-top-nav" icon={faShoppingCart} /> Giỏ hàng</div>
                    </div>
                    
                    <div className="drawer-section">
                        <label className="label-drawer">HỌC HỎI</label>
                        <ul className="drawer-list">
                            <li>Khóa học của tôi</li>
                        </ul>
                    </div>
                    <div className="drawer-section">
                        <label className="label-drawer">PHỔ BIẾN NHẤT</label>
                        <ul className="drawer-list">
                            <li>
                                Web development
                            </li>
                            <li>
                                Mobile App
                            </li>
                            <li>
                                Game development
                            </li>
                            <li>
                                Tất cả các loại
                            </li>
                        </ul>
                    </div>
                    <div className="drawer-section">
                        <label className="label-drawer">DỊCH VỤ</label>
                        <ul className="drawer-list">
                            <li>Tải ứng dụng</li>
                            <li>Dạy trên Udemy</li>
                            <li>Mời bạn bè</li>
                           <Link style = {{textDecoration : 'none',color: '#505763'}} to = '/forgot-password'> <li>Quên mật khẩu</li></Link>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}