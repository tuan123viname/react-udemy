import React, { useState, useRef, useEffect } from 'react';

import './style.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { connect } from 'react-redux';
import Axios from 'axios';
import { rootURL } from '../../constant/Api';
import {  UPDATE_COURSE } from '../../constant/ActionTypes';

const UpdateCourse = ( props ) => {

    const [ course , setCourse ] = useState({
        name        : '',
        goal        : '',
        category    : '',
        price       : 0,
        discount    : 0,
        image       : null,
        description : ''
    });

    useEffect( () => {
        console.log(props.course);
        setCourse( props.course )
    }, [ props.course ]);

    const imageCourse = useRef();

    return (
        <div className = 'new-course-container'>
            <div className = 'new-course-wraper' onClick = { () => props.openModalUpdate(false) }>

            </div>
            <div className = 'new-course-form '>
                <div className = 'title'>
                    <span className='text-title'>Thêm một môn học mới</span>
                    <span onClick = { () => props.openModalUpdate(false)} className = 'close-icon'><FontAwesomeIcon icon = { faTimes } /></span>
                </div>
               <div className = 'form-group'>
                    <input className = 'form-control' value = { course.name } onChange = { ( e ) => setCourse({ ...course, name : e.target.value })} placeholder = 'Tên môn học...' />
                    <select className = 'form-control' value = { course._id }  onChange = { ( e ) => setCourse({ ...course, category : e.target.value })}>
                     { props.categories.map( (item) => <option key = { item._id }  value = { item._id }>{ item.name }</option>) }
                    </select>
                    <input className = 'form-control' placeholder = 'Mục tiêu...' value = { course.goal } onChange = { ( e ) => setCourse({ ...course, goal : e.target.value })} />
                    <textarea className = 'form-control ' placeholder = 'Mô tả...' value = { course.description } onChange = { ( e ) => setCourse({ ...course, description : e.target.value })} />
                    <input className = 'form-control' type = 'number' min = '0' placeholder = 'Giá...' value = { course.price} onChange = { ( e ) => setCourse({ ...course, price : parseInt(e.target.value) })} />
                    <input className = 'form-control' type = 'number' min= '0' max = '100' placeholder = 'Giảm giá(%)...' value = { course.discount } onChange = { ( e ) => setCourse({ ...course, discount : parseInt(e.target.value) })} />
                    <input ref = { imageCourse }  accept="image/*" onChange = { (e) => { setCourse( { ...course, image : imageCourse.current.files[0] })}} type = 'file' className = 'form-control-file'/>
                    <button onClick = { props.updateCourse } className = 'btn btn-success'>Cập nhật</button>
               </div>
                
            </div>
        </div>
    )
}

const mapStateToProps = ( state ) => {
    return { 
        token : state.login.user.token
    }
  
}
const mapDispatchToProps = ( dispatch ) => {
    return {
        updateCourse : ( course ) => dispatch( { type : UPDATE_COURSE , data : course })
    }
}

export default connect( mapStateToProps, mapDispatchToProps )( UpdateCourse );