import React, { useState } from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faAngleDown , faCheck, faCaretRight} from '@fortawesome/free-solid-svg-icons';
import './style.css';

export default function CollapseLesson(props){
    
    const [ collapseActive , setCollapseActive ] = useState('');

    function handleActiveCollapse(id){
        collapseActive == id ? setCollapseActive('') : setCollapseActive(id);
    }

    return (
        <div className="collapse-lesson-container">
            <h3>Nội dung khóa học</h3>
            <div className="collapse-lesson">
           
                <div className="collapse-content">
                    <div className="collapse-content-head" onClick = {() => handleActiveCollapse(1)}>
                        <span className="lesson-title">Làm thế nào để có được dữ liệu bổ sung và bài đọc</span>
                        <span >0/3</span>
                        <span>|</span>
                        <span>3min</span>
                        <span className='icon-collapse'><FontAwesomeIcon icon={faAngleDown} /></span>
                    </div>
                    <div className="collapse-item" style={collapseActive == 1 ? {height: 47*3}: null}>
                        <div>Chương 1 - Mạng lưới thần kinh nhân tạo
                            <span className="lesson-complete-icon"><FontAwesomeIcon icon={faCheck} /></span>
                            <div><span className="icon-play"><FontAwesomeIcon icon={faCaretRight} /> 1 min</span></div>
                        </div>
                        <div>Chương 1 - Mạng lưới thần kinh nhân tạo
                            <span className="lesson-complete-icon"><FontAwesomeIcon icon={faCheck} /></span>
                            <div><span className="icon-play"><FontAwesomeIcon icon={faCaretRight} /> 1 min</span></div>
                        </div>
                        <div>Chương 1 - Mạng lưới thần kinh nhân tạo
                            <span className="lesson-complete-icon"><FontAwesomeIcon icon={faCheck} /></span>
                            <div><span className="icon-play"><FontAwesomeIcon icon={faCaretRight} /> 1 min</span></div>
                        </div>
                        
                    </div>
                </div>

                <div className="collapse-content">
                    <div className="collapse-content-head" onClick = {() => handleActiveCollapse(2)}>
                        <span className="lesson-title">Làm thế nào để có được dữ liệu bổ sung và bài đọc</span>
                        <span >0/3</span>
                        <span>|</span>
                        <span>3min</span>
                        <span className='icon-collapse'><FontAwesomeIcon icon={faAngleDown} /></span>
                    </div>
                    <div className="collapse-item" style={collapseActive == 2 ? {height: 47*3}: null}>
                        <div>Chương 1 - Mạng lưới thần kinh nhân tạo
                            <span className="lesson-complete-icon"><FontAwesomeIcon icon={faCheck} /></span>
                            <div><span className="icon-play"><FontAwesomeIcon icon={faCaretRight} /> 1 min</span></div>
                        </div>
                        <div>Chương 1 - Mạng lưới thần kinh nhân tạo
                            <span className="lesson-complete-icon"><FontAwesomeIcon icon={faCheck} /></span>
                            <div><span className="icon-play"><FontAwesomeIcon icon={faCaretRight} /> 1 min</span></div>
                        </div>
                        <div>Chương 1 - Mạng lưới thần kinh nhân tạo
                            <span className="lesson-complete-icon"><FontAwesomeIcon icon={faCheck} /></span>
                            <div><span className="icon-play"><FontAwesomeIcon icon={faCaretRight} /> 1 min</span></div>
                        </div>
                        
                    </div>
                </div>

                
            </div>
        </div>
    )
}