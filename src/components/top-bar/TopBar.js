import React from 'react';
import './style.css';

export default function TopBar(props){
    const categories = props.categories;
    return (
        <ul className="top-bar">
           { categories.map((item, index) => <li key = { item._id + 'c' }>{ item.name }</li>) }
        </ul>
    )
}