import React, { useState, useEffect, useRef } from 'react';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope, faLock,faUser, faCalendarAlt, faPhone, faNeuter, faInfo } from '@fortawesome/free-solid-svg-icons';
import './style.css';
import Loading from '../loading/Loading';

 function SignupForm( props ){
   
    useEffect(() => {
       if( props.message || props.error )
       {
        handleAlert();
       }
      
    } , [ props.message , props.error])
    const [ user , setUser ] = useState( {
        name : '', 
        email : '',
        phone : '',
        gender : '',
        password : '',
        password1 : '',
        description : '',
        address : '',
        image : ''
    })
    const [ displayAlert1, setDisplayAlert1 ] = useState('none');
    const [ displayAlert2, setDisplayAlert2 ] = useState('none');
    function handleAlert(){
        if( props.error )
        {
            setDisplayAlert2('block');
            setTimeout(() => {
                setDisplayAlert2('none');
            }, 3000);
        }
        else
        {
            setDisplayAlert1('block');
            setTimeout(() => {
                setUser({
                    name : '', 
                    email : '',
                    phone : '',
                    gender : '',
                    password : '',
                    password1 : '',
                    description : '',
                    address : '',
                    image : ''
                });
                setDisplayAlert1('none');
            }, 3000);
        }
    }

    const inputFile = useRef();

    function ValidateEmail(email) 
    {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email  ))
    {
        return (true)
    }
        return (false)
    }

    function handleSignup(){
      const isValidEmail = ValidateEmail( user.email );
      if( !isValidEmail )
      {
          alert ('Email của bạn không hợp lệ')
          return;
      }
      if( !user.name || !user.phone || !user.description || !user.gender || !user.image )
      {
          alert ('Bạn vui lòng nhập đủ thông tin');
          return;
      }
      if( user.password.length < 6)
      {
          alert('Mật khẩu phải từ 6 ký tự trở lên');
          return;
      }
      if (user.password !== user.password1)
      {
          alert('Mật khẩu không trùng khớp');
          return;
      }
      const formData = new FormData();
    
      for ( let key in user)
      {
        formData.append( key , user[ key ] );
      }
      props.signup(formData);
    }

    return (
        <div className="signup-box" style={{display:props.display ? 'block' : 'none'}}>
            <div className="signup-form  form-group">
                <h3>Đăng ký để tham gia vào khóa học ngay</h3>
                 <div className = " alert alert-success" style = {{ display : displayAlert1}}>{ props.message }</div>
                 <div className = " alert alert-danger" style = {{ display : displayAlert2}}>{ props.error }</div>
                <div className="form-control">
                   <FontAwesomeIcon className="icon-signup-form" icon={faUser}/> <input className="signup-input" value = { user.name } onChange = { e => setUser( {...user, name : e.target.value} )} placeholder="Họ và tên ..." />
                </div>
                <div className="form-control">
                    <FontAwesomeIcon icon={faNeuter} className="icon-signup-form" />
                    <select onChange = { e => setUser({ ...user, gender : e.target.value}) } className='signup-input ml-2' value = { user.gender }>
                        <option value="">Giới tính</option>
                        <option value="Nam">Nam</option>
                        <option value ="Nữ">Nữ</option>
                    </select>
                </div>
                <div className="form-control" >
               <FontAwesomeIcon className="icon-signup-form" icon={faPhone}/><input value = { user.phone } onChange = { (e) => setUser( {...user, phone: e.target.value})} className="signup-input" placeholder="Số điện thoại..." />
                </div>
               <div className="form-control">
               <FontAwesomeIcon className="icon-signup-form" icon={faEnvelope}/><input value={ user.email } onChange = { (e) => setUser( {...user, email: e.target.value})} className="signup-input" type="email" placeholder="Email..." />
               </div>
               <div className="form-control">
               <FontAwesomeIcon className="icon-signup-form ml-2" value={ user.description } icon={faInfo}/><input  onChange = { (e) => setUser( {...user, description : e.target.value})} className="signup-input" type="email" placeholder="Đôi nét về bạn..." />
               </div>
               <div className="form-control">
               <FontAwesomeIcon className="icon-signup-form" icon={faLock}/><input value ={ user.password } onChange = { (e) => setUser( { ...user, password : e.target.value })} className="signup-input" type="password" placeholder="Mật khẩu..." />
               </div>
               <div className="form-control">
               <FontAwesomeIcon className="icon-signup-form" icon={faLock}/><input value = { user.password1 }  onChange = { (e) => setUser( {...user, password1: e.target.value})}  className="signup-input" type="password" placeholder="Xác nhận mật khẩu..." />
               </div>
                <input className="input-file" accept="image/*" ref = { inputFile }  onChange = { () =>  setUser( { ...user, image: inputFile.current.files[0] }) }  type="file" />
               <div className="form-control">
                   <button className="btn btn-signup" onClick= {handleSignup}>Đăng ký</button>
               </div>
               
               <Loading  loading = { props.loading } />

            </div>
        </div>
    )
}
const mapStateToProps = ( state ) => {
    return {
        loading : state.loading.loading,
        message : state.user.message,
        error : state.user.error
    }
}
export default connect(mapStateToProps,null)(SignupForm);