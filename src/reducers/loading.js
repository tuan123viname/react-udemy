import * as types from './../constant/ActionTypes';

const initialState = {
   loading : false
}

export default function LoadingReducer (state = initialState , action){
  
    switch (action.type)
    {
        case types.LOADING : 
            return { loading : action.data};
        default : 
            return state;
    }
}