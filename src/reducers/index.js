import { combineReducers } from 'redux';
import user from './user';
import loading from './loading';
import login from './login';
import course from './course';
import category from './category';
const rootReducer = combineReducers({ user , loading , login , course, category });
export default rootReducer;