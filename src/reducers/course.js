import * as types from '../constant/ActionTypes';

const initState = {
    listCourse : [],
    error : null
}

const CourseReducer = (state = initState, action ) => {
    switch ( action.type)
    {
        case types.GET_LIST_COURSE_SUCCESS : 
            return { ...initState, listCourse : action.data };
        case types.GET_LIST_COURSE_FAILURE : 
            return { ...initState, error : action.error };
        case types.ADD_COURSE : 
            return { error : null, listCourse : [ ...state.listCourse, action.data ] };
        case types.UPDATE_COURSE : 
            const newList = state.listCourse.filter( ( item ) => item._id !== action.data._id );
            return { error : null, listCourse : [ newList, action.data ]};
        case types.DELETE_COURSE : 
            const listCourse = state.listCourse.filter( item => item._id !== action.data );
            return { error : null, listCourse : [ ...listCourse ] }
        default : 
            return state;
    }
}
export default CourseReducer;