import * as types from './../constant/ActionTypes';

const initialState = {
    message : '',
    error : null
}

export default function UserReducer (state = initialState , action){
  
    switch (action.type)
    {
        case types.CREATE_USER_PENDING : 
            return { ...state};
        case types.CREATE_USER_SUCCESS : 
            return { ...state, message : action.data };
        case types.CREATE_USER_FAILURE : 
            return { ...state, error : action.error };
        default : 
            return state;
    }
}