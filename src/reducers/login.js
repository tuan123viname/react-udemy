import * as types from '../constant/ActionTypes';
const userLocal = JSON.parse ( localStorage.getItem('user'));
const initialState = {
    isLogedIn : userLocal ? true : false ,
    user : userLocal ? userLocal : {},
    error : null,
    message : '',
}

export default function loginReducer( state = initialState , action) {
    switch (action.type)
    {
        case types.LOG_IN_SUCCESS : 
            localStorage.setItem( 'user',JSON.stringify (action.data));
            return { ...state, isLogedIn : true, user : action.data };

        case types.LOG_IN_FAILURE : 
            return { ...state , error : action.error , isLogedIn: false};

        case types.LOG_OUT_SUCCESS : 
            localStorage.removeItem( 'user' );
            return { ...state, error : null, isLogedIn : false, user : {} };

        case types.UPDATE_USER_SUCCESS : 
            localStorage.setItem( 'user',JSON.stringify (action.data));
            return { ...state, user : action.data , message : 'Cập nhật thành công'};

        case types.UPDATE_USER_FAILURE : 
            return { ...state , error : action.error }; 

        case types.ACTIVE_USER_SUCCESS :
            console.log('vo active success');
            localStorage.setItem( 'user',JSON.stringify (action.data)); 
            const userTemp  = action.data;
            userTemp.token = state.user.token;
            return { ...state , user : userTemp };

        case types.ACTIVE_USER_FAILURE : 
        console.log('vo active fail');
            return { ...state , error : action.error };

        default : 
            return state;
    }
}