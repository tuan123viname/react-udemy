import * as types from '../constant/ActionTypes';

const initialState = { 
    categories : [], 
    error : null
}
const categoryReducer = ( state = initialState , action ) => {
    switch ( action.type ){
        case types.GET_CATEGORIES_SUCCESS : 
            return { ...state , categories : action.data };
        case types.GET_CATEGORIES_FAILURE : 
            return { ...state , error : action.error };
        default : 
            return state;
    }
}
export default categoryReducer;