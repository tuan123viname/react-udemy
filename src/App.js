import React from 'react';
import {
    Switch,
    Route,
    HashRouter
  } from "react-router-dom";
import { connect } from 'react-redux';

import 'bootstrap/dist/css/bootstrap.min.css';
import { Home, Profile, Course, ForgotPassWord, MyCourses } from './pages';
import Lecture from './pages/lecture';

function Routing( props )
{
    return (
        <HashRouter>
            <div>
                <Switch>
                    <Route exact path="/">
                        <Home />
                    </Route>
                    <Route path="/profile/:tab">
                        <Profile />
                    </Route>
                    <Route path="/course">
                        <Course />
                    </Route>
                    <Route path = '/forgot-password'>
                        <ForgotPassWord />
                    </Route>
                    <Route path = '/lecture/:lectureID' >
                        <Lecture />
                    </Route>
                    <Route path = '/my-courses'>
                        <MyCourses />
                    </Route>
                </Switch>
            
            </div>
        </HashRouter>
    )
}


const mapSateToProps = ( state ) => {
    return {
        user : state.login.user,
        isLogedIn : state.login.isLogedIn
    }
}

export default connect( mapSateToProps, null )( Routing )