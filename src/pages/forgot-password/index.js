import React, { useState } from 'react';

import './style.css';
import { ValidateEmail } from '../../helper';
import Axios from 'axios';
import { rootURL } from '../../constant/Api';
import { Loading } from '../../components';
import { Link } from 'react-router-dom';

function ForgotPassword(props){

    const [ email , setEmail ] = useState('');
    const [ password , setPassword ] = useState('');
    const [ token , setToken ] = useState('');
    const [ display , setDisplay ] = useState('block');
    const [ isLoading , setIsLoading ] = useState (false);
    function handleEnter( e )
    {
        if(e.charCode == 13)
        {
            if( !ValidateEmail(email) )
            {
                alert ('Email không hợp lệ');
                return ;
            }
            setIsLoading(true);
            Axios.post ( rootURL + '/forgot-password', { email })
            .then( res => {
                if(res.status == 200)
                {
                    setDisplay('none');
                }
                setIsLoading(false);
            })
            .catch (err => {
                setIsLoading(false);
                alert( err.response.data.message );
            })
        }
    }

    function handleUpdatePassword(){
        setIsLoading(true);
        Axios.post( rootURL + '/reset-password' , { email : email , password : password , token})
        .then( res => {
            if(res.status == 200)
            {
                setIsLoading(false);
                alert('Cập nhật mật khẩu thành công');
                
            }
            else{
                setIsLoading(false);
                alert('Cập nhật mật khẩu thất bại')
            }
        })
        .catch (err => {
            setIsLoading(false);
            alert (err.response.data.message );
        })
    }

    return (
        <div className = 'forgot-password' >
            <div className = "form-group">
                <h3 className = "heading ">Quên mật khẩu </h3>
                <label style = {{display : display}} >Bạn vui lòng nhập email của bạn</label>
                <input type="text" className = " form-control " style = {{display : display}} value = { email } onChange = { (e) => setEmail(e.target.value) } onKeyPress = {(e) => handleEnter(e)} placeholder="Email..." />
                {display == 'none' ? <div className = 'input-margin'>
                    <input type = "text" className = 'form-control' onChange = { (e) => setToken(e.target.value) } placeholder =' token ' />
                    <input type = 'text' onChange = { (e) => setPassword(e.target.value) } className = 'form-control' placeholder = 'Mật khẩu mới' />
                    <button className = 'btn btn-success mt-3' onClick = { handleUpdatePassword }>Cập nhật</button>
                    <Link to = '/' className = 'btn btn-primary ml-3 mt-3' >Về trang chủ</Link>
                </div> : null}
            </div>
            <Loading loading = { isLoading } />
        </div>
    )
}
export default ForgotPassword;