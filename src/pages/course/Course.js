import React from 'react';
import {Header,Footer, CollapseLesson} from '../../components';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { faHeart, faPlay,faCheck} from '@fortawesome/free-solid-svg-icons';
import StarRatings from 'react-star-ratings';
import './style.css';

export default function Course(props){
    return (
        <div>
            <Header />


            <div className="course-container">

                <div className="course-banner">
                    <p style = {{ textAlign: 'right',paddingRight: '80px',paddingTop:'20px'}}><FontAwesomeIcon icon={faHeart} color="white"/> Yêu thích khóa học này</p>
                    <div className="banner-content">
                        <h2>Giới thiệu về Machine Learning</h2>
                        <div>Tìm hiểu cách tạo các mô hình Machine Learning mạnh mẽ và tạo giá trị trên Python. Mẫu mã bao gồm. </div>
                        <div><StarRatings starDimension="15px" starSpacing="2px" changeRating = {props.changeRating} starRatedColor="#f4c150"  rating = {4.3} numberOfStars={5} /><strong> {4.3}</strong><span style = {{ color: 'white',fontSize: '14px'}}>(30)</span> <span style={{marginLeft:'20px'}}>6000 nguoi dang ky</span></div>
                        <div>Được tạo bởi : vu ngoc tuan</div>
                        <div>Cập nhật lần cuối ngày 11/6/2020</div>
                    </div>
                </div>

                <div className="box-course-right">
                    <div className = "box-course-thumbnail">
                        <img src = "https://i.udemycdn.com/course/240x135/1463052_faa1.jpg" />
                            <span className="box-course-play"><FontAwesomeIcon  icon = {faPlay} /></span>
                        <div className="thumbnail-label">Xem truoc khoa hoc nay</div>
                    </div>
                    <div className="box-course-content">
                        <div className="box-course-price">
                        <label className = "new-price">$5000</label>
                            <label className ="old-price">$10000</label>
                           
                        </div>
                        <div className="btn-box-course">
                            <button className="btn btn-block btn-danger">Thêm vào giỏ</button>
                            <button className="btn btn-block btn-success">Mua ngay</button>
                        </div>
                        <ul className="box-course-info">
                            <li>Khóa học này bao gồm</li>
                            <li>Video 18,5 giờ theo yêu cầu</li>
                            <li>Truy cập không giới hạn</li>
                            <li>Có chứng chỉ đào tạo</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div className="what-you-get">
                <h3>Bạn sẽ học được gì</h3>
                <div className="what-you-get_content">
                    <div className="what-you-get_item">
                         <span><FontAwesomeIcon icon={faCheck} /></span>
                        Hiểu được trực giác đằng sau mạng lưới thần kinh nhân tạo
                    </div>
                    <div className="what-you-get_item">
                         <span><FontAwesomeIcon icon={faCheck} /></span>
                        Hiểu được trực giác đằng sau mạng lưới thần kinh nhân tạo
                    </div>
                    <div className="what-you-get_item">
                         <span><FontAwesomeIcon icon={faCheck} /></span>
                        Hiểu được trực giác đằng sau mạng lưới thần kinh nhân tạo
                    </div>
                    <div className="what-you-get_item">
                         <span><FontAwesomeIcon icon={faCheck} /></span>
                        Hiểu được trực giác đằng sau mạng lưới thần kinh nhân tạo
                    </div>
                    <div className="what-you-get_item">
                         <span><FontAwesomeIcon icon={faCheck} /></span>
                        Hiểu được trực giác đằng sau mạng lưới thần kinh nhân tạo
                    </div>
                    <div className="what-you-get_item">
                         <span><FontAwesomeIcon icon={faCheck} /></span>
                        Hiểu được trực giác đằng sau mạng lưới thần kinh nhân tạo
                    </div>
                    <div className="what-you-get_item">
                         <span><FontAwesomeIcon icon={faCheck} /></span>
                        Hiểu được trực giác đằng sau mạng lưới thần kinh nhân tạo
                    </div>
                </div>
            </div>
            <div className="collapse-lesson-section">
                <CollapseLesson />
            </div>
           

            <Footer />
        </div>
    )
}