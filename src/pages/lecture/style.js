import { makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles ( {
    paperLecture : {
        flexGrow : 1,
        width: '100%',
    } 
})