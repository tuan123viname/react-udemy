import React , { useState, useEffect } from 'react';
import { Tab , Tabs ,Paper} from '@material-ui/core';

import { useStyles } from './style';
import './style.css';
import { Video, TableContent, Header, Footer, TabPanel , Exercise, CourseDiscuss } from '../../components';
import { useParams, useLocation } from 'react-router-dom';

export default function Lecture(props)
{
    const innerWidth = document.body.offsetWidth;
    const hash = useLocation().hash.substr(1);
    const classes = useStyles();
    const [value, setValue] = useState(0);
    const [ openExercise , setOpenExercise ] = useState( false );
    useEffect( () => {
      switch ( hash ){
        case 'summary' : 
          return setValue( 0 );
        case 'q&a' : 
          return setValue( 1 );
        case 'exercise' : 
          return setValue( 2 );
        case 'table_content' : 
          return setValue( 3 );
        default : 
          return setValue( 0 );
      }
      
    }, [ hash ])

    const handleChange = (event, newValue) => {
      console.log(newValue);
      setValue(newValue);
    };
    return <div>
        <Header />
        <div  className = 'lecture-container'>
          <div className = ' lecture-video '><Video height = { innerWidth > 600 ? 500 : 300 }/></div> 
         {
           innerWidth > 800 ?
            <div className = 'lecture-table-content'><TableContent /></div> 
           : null
         } 
           
        </div>
        <div className = "lecture-tab">
            <Paper className =  { classes.paperLecture } >
                <Tabs
                    value = { value }
                    onChange = { handleChange }
                    indicatorColor="primary"
                    textColor="primary"
                >
                    <Tab label="Tổng quan " />
                    <Tab label="Hỏi và đáp" />
                    <Tab label="Bài tập trắc nghiệm" />
                  {
                    innerWidth > 800 ? null :  <Tab label = "Nội dung khóa học" />
                  } 
                </Tabs>
            </Paper>
            <TabPanel value = { value } index = {0} >Tổng quan</TabPanel>
            <TabPanel value = { value } index = {1} >
                  <CourseDiscuss />
            </TabPanel>
            <TabPanel value = { value } index = {2} >
                <p>
                Bài tập là phần quan trọng bổ sung cần thiết của việc học. Nó giúp củng cố những kiến thức đã học (hoặc cần được học), giúp các kiến thức đó được hiểu sâu sắc hơn và mở rộng kiến thức.
                </p>
                <button onClick = { () => setOpenExercise(true) }  className = 'btn btn-success' >Bắt đầu làm bài tập</button>
                <Exercise isOpen = { openExercise } setIsOpen = { setOpenExercise } />
            </TabPanel>
            {
              innerWidth >800 ? null : <TabPanel value = { value } index = { 3 } ><TableContent /></TabPanel>
            }
            
        </div>
        <Footer />
    </div>
}