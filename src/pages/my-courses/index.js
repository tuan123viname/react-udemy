import React, { useEffect, useState } from 'react';

import { Header, Footer, CourseCardTeaching, AddNewCourse, UpdateCourse } from '../../components';
import './style.css';
import { connect } from 'react-redux';
import { GET_LIST_COURSE, DELETE_COURSE } from '../../constant/ActionTypes';
import Skeleton from '@material-ui/lab/Skeleton';

function MyCourses(props){
    const [ openModal , setOpenModal ] = useState( false );
    useEffect( () => {
        props.getListCourse( props.idUser );
    },[])

    return (
        <div>
            <Header />
            <div className = 'course-teaching-container'>
                <div className = 'course-teaching-head'>
                    <h4>Các khóa học của bạn</h4>
                    <div className = 'course-teaching-head-right'>
                        <select className="form-inline">
                            <option>Sort by</option>
                            <option>Name</option>
                            <option>Price</option>
                        </select>
                        <button onClick = { () => setOpenModal(true) } className='btn btn-success'>New Course</button>
                    </div>
                </div>
                <div className = 'list-card-teaching'>
                    {
                       props.listCourse.length == 0 ? [ 1,2,3,4 ].map(item => <Skeleton key = { item } variant = 'rect' width = { 210 } height = { 250 } /> )  :
                        props.listCourse.map( (item,index) => <div key = { 'item' + index } className = 'item-teaching'>
                        <CourseCardTeaching categories = { props.categories } token = { props.token } updateCourse = { props.updateCourse } deleteCourse = { props.deleteCourse } course = { item } />
                    </div> )
                    }
               </div>
                
             
            </div>
            { openModal ?  <AddNewCourse categories = { props.categories } setOpenModal = { setOpenModal } /> : null }
            <Footer />
        </div>
    )
}

const mapStateToProps = ( state ) => {
    return {
        listCourse : state.course.listCourse,
        error : state.course.error,
        token : state.login.user.token,
        categories : state.category.categories,
        idUser : state.login.user._id
    }
}

const mapDispatchToProps = (dispatch) => {
    return { 
        getListCourse : ( idUser ) => dispatch({ type : GET_LIST_COURSE , idUser : idUser }),
        deleteCourse : ( idCourse ) => dispatch( { type : DELETE_COURSE, data : idCourse } )
    }
}

export default connect( mapStateToProps, mapDispatchToProps )(MyCourses);