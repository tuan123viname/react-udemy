import Home from './home/Home';
import Profile from './profile/Profile';
import Course from './course/Course';
import ForgotPassWord from './forgot-password';
import MyCourses from './my-courses';
export{
    Home,
    Profile,
    Course,
    ForgotPassWord,
    MyCourses
}