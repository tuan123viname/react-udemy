import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import {Header, Footer, TopBar,Carousel} from '../../components/index';
import './style.css';
import Axios from 'axios';
import { rootURL } from '../../constant/Api';
import { GET_CATEGORIES } from '../../constant/ActionTypes';

function Home (props){

    const [ freeCourses, setFreeCourses ] = useState([]);
    const [ topCourses , setTopCourses ] = useState([]);
    
    useEffect( () => {
        Axios.get( rootURL + '/course/get-top')
        .then( res => {
            if( res.status == 200)
            {
                console.log(res.data);
                setTopCourses(res.data);
            }
        })
        .catch(err => {
            console.log(err);
        })

        Axios.get( rootURL + '/course/get-free')
        .then(res => {
            if(res.status == 200)
            {
                console.log(res.data);
                setFreeCourses(res.data);
            }
        })
        .catch(err => {
            console.log(err);
        })

    },[])

    return (
        <div>
            <Header />
            <TopBar categories = { props.categories } />
            <div className="container">
                <br />
                <h5>Khóa học phổ biến nhất</h5>
                <Carousel listCourse = { topCourses } />
                <br />
                <h5>Khóa học Miễn phí</h5>
                <Carousel listCourse = { freeCourses } />
            </div>
            
            <Footer />
        </div>
    )
}

const mapStateToProps = ( state ) => {
    return {
        categories : state.category.categories
    }
}
const mapDispatchToProps = ( dispatch ) => {
    return {
       
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);