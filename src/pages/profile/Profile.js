import React, { useState ,Fragment, useEffect} from 'react';
import { Redirect , useParams } from 'react-router-dom';
import { connect } from 'react-redux';
import {Header,Profile as ProfileComponent, Footer, Loading} from '../../components/index';
import './style.css';
import { ACTIVE_USER, UPDATE_USER } from '../../constant/ActionTypes';

function Profile (props){
    const { tab } = useParams();
    const [ activeTab, setActiveTab ] = useState( tab );
    console.log('user in profile'+ JSON.stringify( props.user ));
    useEffect (() => {
        setActiveTab( tab );
    }, [ tab ]);
 
    return (
       
        <div>
           
            { 
                props.isLogedIn ?
                <Fragment>
                    <Header />
                    <br/>
                    <ProfileComponent activeAccount = { props.activeAccount } error = { props.error }  message = { props.message } updateUser = { props.updateUser } userInfo={ props.user } activeTab={ activeTab } courses = {[]} handleActiveTab = { setActiveTab } />
                    <Footer /> 
                    <Loading loading = { props.loading} />
                </Fragment> 
                : 
                <Redirect to = "/" />
            }
        </div>
    )
}

const mapStateToProps = ( state ) => {
    return {
        user : state.login.user,
        isLogedIn : state.login.isLogedIn,
        message : state.login.message,
        loading : state.loading.loading,
        error : state.login.error
    }
}
const mapDispatchToProps = (dispatch ) => {
    return {
        updateUser : ( userData  ) => dispatch({ type : UPDATE_USER, data : userData}), 
        activeAccount : ( data ) => dispatch ({ type : ACTIVE_USER, data })
    }
}

export default connect( mapStateToProps, mapDispatchToProps )(Profile);